# Monumento iPad

## Student - Aaryan Kothari
## Links  
- Project Code: https://gitlab.com/aossie/monumento-ipad
- Testflight: https://testflight.apple.com/join/UNc2lxUo
- Working Demo: https://drive.google.com/file/d/1376Iy_0BnmLev5vYcfE_HI5DDoga9sou/view?usp=sharing
- Designs: https://www.figma.com/file/srodTGaAXcT9fKiBVoGy2M/Monumento?node-id=201%3A2

## Monumento 

The goal of the project is to provide users an unique experience of exploring and learning more about the various monumental structures all around the world within the app. It's core feature involves detecting a monument from its image, letting users know its name and Wikipedia details all from within the app along with visualizing a 3D model of it infront of them. The app has features like Popular Monuments, discovering and following your friends, liking their posts on your feed and much more!

I decided to build this project in SwiftUI. Although SwiftUI being a relatively new framework, did cause few limitations in developing a production grade application, It does increase a lot of scope for the future of Monumento.  

### Prominent Feature list 

1. Integrated Firebase and GCP projects with app - **Done**
2. User Authentication, Login Screen,Sign-up screen (email / google / apple auth) - **Done** 
3. User Registeration ( signup form ) - **Done**
5. Home Screen, Popular Monuments, Explore, Bookmarks  - **Done** 
6. Monument Detector Screen integrated with Cloud Vision API for on cloud processing - **Done** 
7. Data fed and structured in Firestore as database for app - **Done** 
8. Discover screen to search users. - **Done**
9. Developed Wikipedia feature, Sliding up panel integrated for Wikipedia, Data sharing across Detector and AR screens, Wiki page according to monument in realtime - **Done** 
10. DiscoverView to find other users using the app - **Done**
11. View other user profiles and share / follow them - **Done**
12. Detail Screen for Monuments with Animation - **Done**
13. Wikipedia for Popular Monuments - **Done**
14. Visualize Popular Monuments in AR directly - **Done**
15. Bookmark Monuments, DB/Backend integration for Bookmarks - **Done**
16. Unique User id Authentication logic added for bookmarks collection - **Done**
17. Profile Screen, User details while Sign Up feature - **Done**
18. FeedView for viewing posts.
19. Posts can be liked, commented on and shared - **Done**
20. Complete Social media experience with AR and ML to detect and visualise monuments! - **Done**

### Deep view into the technology. 

This project has been developed with [SwiftUI](https://developer.apple.com/xcode/swiftui/). It makes use of minimal open source libraries available for SwiftUI. Some of which are listed below

* [Firebase](https://firebase.google.com) - Provides Authentication Support for app, Firestore as backend/database support.
* [Google Sign-In](https://developers.google.com/identity/sign-in/ios) - Provides sign in support with Google.
* [Kingfisher](https://github.com/onevcat/Kingfisher) - Provides support for asynchronous downloading and caching of remote images.

### All Merge Requests 
1. [ Merge request !1](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/1) - Initialize repository: - status *Merged*
    * Initialized the respository with issue templates, MR templates, gitignore and xcode project. 

2. [Merge request !2](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/2) - Setup project - status *Merged*
    * Add dependencies, setup firebase and apple entitlements.

3. [Merge request !3](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/3) - LoginView - Status: *Merged*
    * Implemeted email auth, google auth and apple auth with login and signup screens of the application.

4. [Merge request !4](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/4) - Registeration Form - Status: *Merged*
    * Implemented registeration form for new users and added Firestore combine classes for sending/receiving data.

5. [Merge request !5](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/5) - MonumentsView - Status: *Merged*
    * Implemented the main screen of the app: MonumentsView which displays the list of all monuments.
    * Implemented MonumentDetailView which presents the map, images, and description of chosen monuments.

6. [Merge request !6](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/6) - FeedView - Status: *Merged*
    * Implemented first part of the social media feature of the app: FeedView.
    * Users can view, like and share on posts made by their friends.

7. [Merge request !7](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/7) - CommentsView - Status: *Merged*
    * Part 2 of social media feature.
    * Users can comment on posts on their feed.

8. [Merge request !8](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/8) - ProfilView - Status: *Merged*
    * Implemented Profile Screen interface where users can view their own profile.
    * Implemented Profile Screen interface where users can view other profiles and send a follow request.

9. [Merge request !9](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/9) - AddPostView - Status: *Merged*
    * Part 3 of social media feature.
    * Implemented screens for users to add their own posts to appear on their friends screen.

10. [Merge request !11](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/11) - Navigation - Status: *Merged*
    * Added respective navigationViews and TabViews to bind the application together.

11. [Merge request !12](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/12) - Monument Detection - Status: *Merged*
    * Implemented Cloud Vision API to detect monuments and present the details of the monument.

12. [Merge request !13](https://gitlab.com/aossie/monumento-ipad/-/merge_requests/13) - DiscoverView - Status: *Merged*
    * Part 4 and final part of social media feature.
    * Implemented discoverView, a place where users can find their friends to send a follow request.

I would like to thank the whole AOSSIE community, especially my mentor Jaideep Prasad for being so nice and helpful. I have learnt a lot in the past 3 months and it has been a great experience to be a part of this wonderful community and develop this awesome project.
