//
//  AuthProtocol.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 30/06/21.
//

import Combine

protocol AuthServiceProtocol {
    static func login(email: String, password: String) -> AnyPublisher<String, Error>
    static func signup(email: String, password: String) -> AnyPublisher<String, Error>
    static func logout() -> AnyPublisher<Void, Error>
}
