//
//  Comment.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 24/07/21.
//

import Foundation

typealias Comments = [Comment]

struct Comment: Codable, Identifiable {
    var id = UUID()
    var author: User?
    var post: String?
    var comment: String?
    var timeStamp: Double?
    
    enum CodingKeys: String, CodingKey {
        case author
        case post = "postInvolvedId"
        case comment
        case timeStamp
    }
    
    var asDictionary: [String:Any] {
        var dict: [String:Any] = [:]
        dict["postInvolvedId"] = post ?? ""
        dict["comment"] = comment ?? ""
        dict["timeStamp"] = Date().timeIntervalSince1970
        dict["author"] = User.current.asDictionary
        return dict
    }
    
    
    static let data = Comment(author: User(firstName: "Aaryan", lastName: "Kothari", username: "aaryan.kotharii", status: "YOLO", profilePicture: "https://cdn.mos.cms.futurecdn.net/wtqqnkYDYi2ifsWZVW2MT4-1200-80.jpg"), post: "9282929829", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit amet bibendum turpis, sit amet faucibus ex. Nam sollicitudin ultricies auctor. Integer gravida blandit semper.", timeStamp: 19287287282)
}


