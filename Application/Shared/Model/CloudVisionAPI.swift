//
//  CloudVisionAPI.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 09/08/21.
//

import Foundation

struct CloudVisionAPI {
    
    struct Response: Codable {
        let responses: [CloudVisionResponse]?
    }
    
    struct CloudVisionResponse: Codable {
        let landmarkAnnotations: [LandmarkAnnotation]?
    }
    
    struct LandmarkAnnotation: Codable {
        let description: String?
        let locations: [Location]?
        
        enum CodingKeys: String, CodingKey {
            case description
            case locations
        }
        
        var coordinates: [Double] {
            let lat = self.locations?.first?.latLng?.latitude ?? 0.0
            let long = self.locations?.first?.latLng?.longitude ?? 0.0
            return [lat,long]
        }
    }
    
    struct Location: Codable {
        let latLng: LatLng?
    }
    
    struct LatLng: Codable {
        let latitude, longitude: Double?
    }
    
    struct Request: Codable {
        let requests: [CloudVisionRequest]
        
        init(with image:String) {
            self.requests = [CloudVisionRequest(image: Images(content: image), features: [Feature(maxResults: 10, type: "LANDMARK_DETECTION")])]
        }
    }
    
    struct CloudVisionRequest: Codable {
        let image: Images
        let features: [Feature]
    }
    
    struct Feature: Codable {
        let maxResults: Int
        let type: String
    }
    
    
    struct Images: Codable {
        let content: String
    }
    
}
