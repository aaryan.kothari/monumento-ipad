//
//  Post.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 21/07/21.
//

import Foundation

typealias Posts = [Post]

struct Post: Codable, Identifiable, FirebaseIdentifiable {
    var id = UUID().uuidString
    var docId: String?
    var author: User?
    var image: String?
    var location: String?
    var timeStamp: Double?
    var title: String?
    
    enum CodingKeys: String, CodingKey {
        case author
        case image = "imageUrl"
        case location
        case timeStamp
        case title
        case docId
    }
    
    var imageUrl: URL? {
        return URL(string: image ?? "")
    }
    
    static let data = Post(author: User(firstName: "Aaryan", lastName: "Kothari", username: "aaryan.kotharii", status: "YOLO"), image: "https://cdn.mos.cms.futurecdn.net/wtqqnkYDYi2ifsWZVW2MT4-1200-80.jpg", location: "Beach", timeStamp: 19287287282, title: "Beach Day!")
}

protocol FirebaseIdentifiable {
    var docId: String? { get set }
}
