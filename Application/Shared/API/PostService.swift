//
//  PostService.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 23/07/21.
//

import Firebase
import Combine

class PostService: ObservableObject {
    private let postsCollection = Firestore.firestore().collection("posts")
    
    private var cancellables: [AnyCancellable] = []
    
    let onErrorCompletion: ((Subscribers.Completion<Error>) -> Void) = { completion in
        switch completion {
        case .finished: print("🏁 finished")
        case .failure(let error): print("❗️ failure: \(error)")
        }
    }
    
    let onValue = { print("✅ value") }
    
    func fetchPosts(completion: @escaping (Posts) -> ()) {
        postsCollection
            .getDocuments(as: Post.self)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: { print($0);completion($0) } )
            .store(in: &cancellables)
    }
    
    func fetchComments(for doc:String,completion: @escaping (Comments) -> ()) {
        guard !doc.isEmpty else { return }
        postsCollection.document(doc).collection("comments")
            .getDocuments(as: Comment.self)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: { print($0);completion($0) } )
            .store(in: &cancellables)
    }
    
    func sendComment(_ comment: Comment) {
        postsCollection.document(comment.post ?? "").collection("comments")
            .addDocument(data: comment.asDictionary) { _ in }
    }
    
    func commentsListener(for doc:String,completion: @escaping (Comments) -> ()) {
        postsCollection.document(doc).collection("comments")
            .publisher(as: Comment.self)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: { print($0); completion($0) } )
            .store(in: &cancellables)
    }
    
    func fetchUserPosts(for uid: String,completion: @escaping (Posts) -> ()) {
        postsCollection.whereField("author.uid", isEqualTo: uid)
            .getDocuments(as: Post.self)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: { completion($0) } )
            .store(in: &cancellables)
    }
    
    func addPost(for post: Post) {
        print(post.dictionary)
        postsCollection
            .addDocument(data: post.dictionary){ _ in }
    }
    
    func uploadPhoto(for data: Data,completion:@escaping (String)->()) {
        let ref = Storage.storage().reference(withPath: "posts/\(UUID().uuidString).png")
        
        let uploadTask = ref.putData(data, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                print("Error: \(error.debugDescription)")
                return
            }
            ref.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    print("Download URL not found")
                    return
                }
                completion(downloadURL.absoluteString)
            }
        }
    }
}
