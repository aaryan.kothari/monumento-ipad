//
//  ProfileViewModel.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 28/07/21.
//

import Foundation
import SwiftUI

class ProfileViewModel: ObservableObject {
    @Published var socialService: PostService = PostService()
    @Published var userService: UserService = UserService()
    @Published var followTitle = "Follow"
    
    @Published var posts: [Post] = []
    @Published var profile: User = User()

    @Published var columns = [GridItem(.flexible(),spacing: 40),GridItem(.flexible(),spacing: 40),GridItem(.flexible(),spacing: 40)]
    
    private var uid: String
    
    init(uid: String) {
        self.uid = uid
        self.getPosts()
        self.getUserDetails()
        print(User.current.following)
        if User.current.following?.contains(uid) ??  false {
            self.followTitle = "Following"
        }
    }
    
    func getPosts() {
        socialService.fetchUserPosts(for: uid) {
            self.posts = $0
        }
    }
    
    func getUserDetails() {
        userService.fetchUser(for: uid) { user in
            if let user = user {
                self.profile = user
            }
        }
    }
    
    func followUser() {
        userService.followUser(with: uid)
        self.followTitle = "Following"
        let user = User.current
        var following = user.following ?? []
        following.append(uid)
        user.following = following
        user.persist { success, error in
            print(success,error)
        }
    }
}
