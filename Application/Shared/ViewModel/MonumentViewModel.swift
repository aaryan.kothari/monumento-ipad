//
//  MonumentViewModel.swift
//  Monumento
//
//  Created by Aaryan Kothari on 15/07/21.
//

import UIKit
import SwiftUI
import Firebase


class MonumentViewModel: ObservableObject {
    @Published var monumentService: MonumentService = MonumentService()
    @Published var monuments: [Monument] = []
    @Published var monument = Monument(coordinate: [])
    @Published var selection: String? = nil
    @Published var popularOn: Bool = false
    @Published var columns = [GridItem(.flexible())]
    @Published var inputImage : UIImage?
    @Published var showingImagePicker: Bool = false
    
    init() {
        self.getMonuments()
    }
    
    func getMonuments() {
        monumentService.fetchMonuments {
            print($0)
            self.monuments = $0
        }
    }
    
    var listHeader: String {
        return popularOn ? "Popular Monuments" : "All Monuments"
    }
    
    var buttonImage: String {
        return popularOn ? "list.dash" : "star.circle.fill"
    }
    
    var buttonTitle: String {
        return popularOn ? "View All":"View Popular"
    }
    
    func toggleButton() {
        withAnimation {
            self.popularOn.toggle()
            columns = popularOn ? [GridItem(.flexible())] : [GridItem(.flexible()),GridItem(.flexible())]
        }
    }
    
    func loadImage(){
        self.inputImage = inputImage
        MonumentDetector(image: inputImage).detect { monument in
            print("GOT MONUMENT: \(monument)")
            self.monument = monument
            self.selection = "ABC"
        }
    }
    
    func showImagePicker() {
        self.showingImagePicker = true
    }
}

