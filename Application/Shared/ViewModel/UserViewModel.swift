//
//  UserViewModel.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 09/07/21.
//

import UIKit
import SwiftUI
import Firebase


class UserViewModel: ObservableObject {
    @Published var profilePicture: UIImage? = nil
    
    @Published var firstName: String = ""
    @Published var lastName: String = ""
    @Published var username: String = ""
    @Published var status: String = ""
    @Published var showAlert: Bool = false
    @Published var alert: StringConstants.Alerts = .error(message: "Error")
    
    @Published var userService: UserService = UserService()
    
    @Published var userCreated: Bool = false
    
    private let uid = Auth.auth().currentUser?.uid ?? "uid"

    var buttonTitle = "SUBMIT"
    
    init() {
        self.userCreated = UserDefaults.standard.bool(forKey: "userExists")
    }
    
    func addUser() {
        guard isValid() else { self.showAlert = true; return }
        self.userCreated = true
        UserDefaults.standard.set(true, forKey: "userExists")
        userService.addUser(User(firstName: firstName, lastName: lastName, username: username, status: status))
    }
    
   
    
    func isValid() -> Bool {
        if firstName.isEmpty {
            self.alert = .error(message: "First name cannot be empty")
            return false
        }
        
        if lastName.isEmpty {
            self.alert = .error(message: "Last name cannot be empty")
            return false
        }
        
        if username.isEmpty {
            self.alert = .error(message: "Username cannot be empty")
            return false
        }
        return true
    }
}
