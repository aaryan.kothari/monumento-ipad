//
//  AKImage.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 08/07/21.
//

import SwiftUI

struct AKImage: View {
    
    @State private var showingImagePicker = false
    @Binding var inputImage : UIImage?
    @State var profileImage : Image = Image("default")
    
    @State private var showingAlert = false
    @State var alertTitle : String = "Uh Oh 🙁"
    
    @State private var showingActionSheet = false
    @State private var sourceType : UIImagePickerController.SourceType = .photoLibrary
    
    var gradient: Gradient
    var clipped: Bool = true
    var body: some View {
        ZStack {
            gradient.topBottomLinear
                .cornerRadius(clipped ? 104 : 7)
                .aspectRatio(1, contentMode: .fit)
                .frame(width: 208, height: 208)
            profileImage
                .resizable()
                .cornerRadius(clipped ? 100 : 7)
                .aspectRatio(1, contentMode: .fit)
                .frame(width: 200, height: 200)
                .onTapGesture(perform: imageTapped)
        }
        .sheet(isPresented: $showingImagePicker,onDismiss: loadImage) {
            ImagePicker(image: self.$inputImage, source: self.sourceType)
        }
        .actionSheet(isPresented: $showingActionSheet) {
            ActionSheet(title: Text(""), buttons: [
                .default(Text("Choose Photo")) {
                    self.sourceType = .photoLibrary
                    self.showingImagePicker = true
                },
                .default(Text("Take Photo")) {
                    self.sourceType = .camera
                    self.showingImagePicker = true
                },
                .cancel()
            ])
        }
    }
    
    func loadImage(){
        guard let inputImage = inputImage else { return}
        profileImage = Image(uiImage: inputImage)
    }
    
    func imageTapped() {
        if UIDevice.isIPad {
            self.sourceType = .photoLibrary
            self.showingImagePicker = true
        } else {
        self.showingActionSheet = true
        }
    }
}

struct AKImage_Previews: PreviewProvider {
    static var previews: some View {
        AKImage(inputImage: .constant(nil), gradient: Gradient(colors: []))
    }
}
