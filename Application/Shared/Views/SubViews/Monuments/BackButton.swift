//
//  BackButton.swift
//  Monumento
//
//  Created by Aaryan Kothari on 18/07/21.
//

import SwiftUI

struct BackButton: View {
    var title: String
    var color: Color = .customOrange
    var image: StringConstants.SystemImages = .backArrow
    var width: CGFloat = 50
    var body: some View {
        HStack {
            Image(systemName: image.rawValue)
                .resizable()
                .frame(width: width, height: width)
                .foregroundColor(color)
                .padding(.leading,25)
            Text(title)
                .font(.system(size: width, weight: .bold))
                .foregroundColor(color)
            Spacer()
        }
    }
}

struct BackButton_Previews: PreviewProvider {
    static var previews: some View {
        BackButton(title: "Back")
    }
}
