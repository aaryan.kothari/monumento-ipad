//
//  CrossButton.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 08/07/21.
//

import SwiftUI

struct CrossButton: View {
    var body: some View {
        HStack {
        Image(systemName: "multiply")
            .resizable()
            .foregroundColor(.white)
            .padding(15)
            .background(Color.customOrange)
            .cornerRadius(8)
            .frame(width: 50, height: 50)
            Spacer()
        }
    }
}

struct CrossButton_Previews: PreviewProvider {
    static var previews: some View {
        CrossButton()
            .previewLayout(.sizeThatFits)
    }
}
