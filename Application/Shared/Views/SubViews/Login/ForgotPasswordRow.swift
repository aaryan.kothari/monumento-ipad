//
//  ForgotPasswordRow.swift
//  Monumento
//
//  Created by Aaryan Kothari on 29/06/21.
//

import SwiftUI

struct ForgotPasswordRow: View {
    var session: SessionStore
    var body: some View {
        HStack {
            RememberMeBlock(isSelected: session.rememberMe)
            
            Spacer()
            
            Button("Forgot Password?", action: session.forgotPassword)
                .foregroundColor(.secondary)
        }
    }
}

struct ForgotPasswordRow_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordRow(session: SessionStore(mode: .login))
            .previewLayout(.sizeThatFits)
    }
}
